﻿using UnityEngine;

public class HitFlash : MonoBehaviour {
	//[SerializeField]
	SpriteRenderer[] targets;

	public Color damageFlashColor;
	public float flashDecaySpeed;

	Color flashColor;
	float flashTime = 0;
	MaterialPropertyBlock block;
	// Use this for initialization

	void Start () {
		targets = GetComponentsInChildren<SpriteRenderer>();
		block = new MaterialPropertyBlock();
	}

	[ContextMenu( "Debug Hit" )]
	void DebugHit () {
		Hit( 1 );
	}

	public void Hit ( float str ) {
		flashTime = str;
	}

	private void Update () {
		foreach( var target in targets ) {
			target.GetPropertyBlock( block );
			flashColor = Color.Lerp( Color.black, damageFlashColor, flashTime );
			block.SetColor( "_Damage", flashColor );
			target.SetPropertyBlock( block );
		}
		flashTime -= flashDecaySpeed * Time.deltaTime;
	}
}
