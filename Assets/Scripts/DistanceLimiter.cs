﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceLimiter : MonoBehaviour {

	[SerializeField]
	BubbleCollider collider;

	[SerializeField]
	SpriteRenderer sprite;

	Vector3 homeScale;
	Vector3 scaleVel;
	// Use this for initialization
	void Start () {
		homeScale = sprite.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		var targetScale = Vector3.zero;
		if( collider.enabled ) {
			targetScale = homeScale;
		}
		sprite.transform.localScale = Vector3.SmoothDamp( sprite.transform.localScale, targetScale, ref scaleVel, 0.05f );
	}

	internal void Enable () {
		collider.enabled = true;
		//sprite.enabled = true;
	}

	internal void Disable () {
		collider.enabled = false;
		//sprite.enabled = false;
	}
}
