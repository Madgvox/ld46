﻿using System;
using TMPro;
using UnityEngine;

public class GameUI : MonoBehaviour {

	public TextMeshProUGUI roundText;
	public TextMeshProUGUI healthText;

	private RoundSequencer currentSeq;

	public ScreenWipe wipe;

	void UpdateRoundText () {
		roundText.text = currentSeq.winMode + " " + currentSeq.currentKillCount + " " + currentSeq.currentHighwater + " " + currentSeq.currentKillTarget;
	}

	internal void UpdateGameData ( Game game) {
		healthText.text = "Your Health: " + game.player.health + "\nTree Health: " + game.tree.health;
	}

	internal void UpdateRoundData ( RoundSequencer seq ) {
		currentSeq = seq;
		UpdateRoundText();
	}
}
