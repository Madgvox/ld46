﻿using UnityEngine;
using UnityEngine.Rendering;

public class PlayerAnimator : MonoBehaviour {
	public SpriteRenderer legs;

	public SpriteRenderer torso;

	public SpriteRenderer mask;
	public SpriteRenderer arms;
	public SortingGroup treeGroup;

	public Sprite frontMask;
	public Sprite backMask;
	public Sprite torsoNormal;
	public Sprite torsoHunched;
	public Sprite torsoNormalBlue;
	public Sprite torsoHunchedBlue;
	public Sprite legsNormal;
	public Sprite legsBlue;

	Vector2 facing;


	Vector3 maskHome;
	Vector3 armHome;
	Vector3 treeHome;
	Vector3 maskVel;
	Vector3 treeVel;

	private void Start () {
		maskHome = mask.transform.localPosition;
		treeHome = treeGroup.transform.localPosition;
		armHome = arms.transform.localPosition;
	}

	public void SetFacing ( Vector2 facing ) {
		this.facing = facing;
	}

	private void Update () {
		if( facing.y > 0 ) {
			mask.sprite = backMask;
			mask.sortingOrder = -1;
			treeGroup.sortingOrder = 2;
		} else {
			mask.sprite = frontMask;
			mask.sortingOrder = 2;
			treeGroup.sortingOrder = -1;
		}

		var maskHeight = maskHome.y;

		if( !Game.instance.player.isHoldingTree && !Game.instance.player.inIntroMode ) {
			legs.sprite = legsBlue;
			torso.sprite = torsoNormalBlue;
		} else {
			legs.sprite = legsNormal;
			if( Game.instance.player.isHoldingTree ) {
				torso.sprite = torsoHunched;
				maskHeight -= 0.1f;
			} else {
				torso.sprite = torsoNormal;
			}
		}

		var target = maskHome;
		target.x *= facing.x;

		mask.transform.localPosition = Vector3.SmoothDamp( mask.transform.localPosition, target, ref maskVel, 0.1f );
		if( mask.transform.localPosition.x < 0 ) {
			torso.flipX = true;
			legs.flipX = true;
		} else {
			torso.flipX = false;
			legs.flipX = false;
		}

		target = treeHome;
		target.x *= facing.x;

		treeGroup.transform.localPosition = Vector3.SmoothDamp( treeGroup.transform.localPosition, target, ref treeVel, 0.1f );


		target = armHome;
		target.x *= facing.x;

		arms.flipX = facing.x < 0;
		arms.transform.localPosition = target; 
		if( !Game.instance.player.isHoldingTree ) {
			arms.enabled = false;
		} else {
			arms.enabled = true;
		}
	}
}
