﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroTrigger : MonoBehaviour {

	private void OnTriggerEnter2D ( Collider2D collision ) {
		if( enabled ) {
			var player = collision.GetComponent<Player>();

			if( player ) {
				Game.instance.IntroCutscene();
				enabled = false;
			}
		}
	}
}
