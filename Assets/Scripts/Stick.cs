﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour, HitSource {
	
	[SerializeField]
	StickHit swingPrefab;

	public LayerMask hitMask;

	public ParticleSystem hitParticle;

	Collider2D[] hitResults = new Collider2D[ 100 ];

	public float damage = 1;
	public float knockbackForce = 10;

	public Vector3 position {
		get {
			return transform.position;
		}
	}

	public float damageAmount {
		get {
			return damage;
		}
	}

	public void Use () {
		if( Input.GetButtonDown( "Use" ) ) {
			Swing();
		}
	}

	private void Swing () {
		var filter = new ContactFilter2D();
		filter.layerMask = hitMask;
		filter.useLayerMask = true;

		var swing = Instantiate( swingPrefab, transform );

		swing.transform.position = transform.position;

		//swing.transform.localScale = new Vector3( -Game.instance.player.roughFacing.x, 1 );

		var cap = (CapsuleCollider2D)swing.collider;
		var offset = Quaternion.AngleAxis( cap.transform.localEulerAngles.z, Vector3.forward ) * cap.offset;
		Debug.DrawRay( cap.transform.position + offset, Vector3.right );
		Debug.DrawRay( cap.transform.position + offset, Vector3.down );
		Debug.DrawRay( cap.transform.position + offset, Vector3.left );
		Debug.DrawRay( cap.transform.position + offset, Vector3.up );
		var count = Physics2D.OverlapCapsuleNonAlloc( cap.transform.position + offset, cap.size, CapsuleDirection2D.Vertical, cap.transform.localEulerAngles.z, hitResults, hitMask );

		if( count > 0 ) {
			for( int i = 0; i < count; i++ ) {
				var c = hitResults[ i ];

				Debug.Log( c );

				var enemy = c.GetComponent<EnemyAI>();
				if( enemy ) {
					var dmgDir = enemy.transform.position - Game.instance.player.transform.position;
					var part = Instantiate( hitParticle );
					var main = part.main;
					var color = new ParticleSystem.MinMaxGradient( new Color32( 24, 144, 97, 255 ) );
					main.startColor = color;
					part.transform.position = enemy.damagePoint.position;
					enemy.TryDamage( this, dmgDir.normalized * knockbackForce );
				}

				var tree = c.GetComponent<Tree>();
				if( tree ) {

				}
			}
		} else {
		}
	}

	public void OnHit () {
		
	}
}
