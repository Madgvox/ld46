﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface AnimationEventReciever {
	void RecieveEvent ( string eventName );
}

public class FlyingEnemyAI : EnemyAI, AnimationEventReciever {
	public float hoverDistance = 4f;
	public float fleeDistance = 2f;
	public float shootDistance = 10f;

	public float shootRate;
	public float bulletSpeed;


	public AudioClip shoot;

	float shootTimer;

	public PhysicsBase bulletPrefab;

	Vector2 lastTargetDir;

	// Update is called once per frame
	protected override void OnFixedUpdate () {
		var target = Game.instance.tree.projectileHitbox.transform;
		if( Game.instance.player.isHoldingTree ) {
			target = Game.instance.player.transform;
		}
		if( target ) {
			Vector2 targetDir = target.position - transform.position;

			var dist = targetDir.magnitude;
			targetDir.Normalize();

			var current = animator.GetInteger( "facing_x" );

			facingX = (int)Mathf.Sign( targetDir.x );

			if( current != facingX ) {
				animator.SetTrigger( "face_change" );
			}
			animator.SetInteger( "facing_x", facingX );

			if( dist > hoverDistance ) {
				velocity = targetDir * moveSpeed;
			} else if( dist < fleeDistance ) {
				velocity = -targetDir * moveSpeed;
			} else {
				var mag = velocity.magnitude;
				mag -= 3f * Time.fixedDeltaTime;
				if( mag < 0 ) mag = 0;
				velocity = velocity.normalized * mag;
			}

			if( dist < shootDistance ) {
				shootTimer += Time.deltaTime;
			} else {
				shootTimer = 0;
			}

			lastTargetDir = targetDir;
			if( shootTimer > shootRate ) {
				shootTimer = UnityEngine.Random.Range( -0.3f, 0.3f );

				animator.SetTrigger( "Shoot" );
				//Shoot( targetDir );
			}
		}
	}

	private void Shoot ( Vector3 direction ) {
		var bullet = Instantiate( bulletPrefab );
		bullet.transform.position = transform.position;
		bullet.velocity = direction * bulletSpeed;
		PlaySound( shoot, 0.4f );
	}

	protected override void OnKill () {
		if( audioSource ) {
			audioSource.transform.parent = null;
			Destroy( audioSource.gameObject, 5f );
		}
		PlaySound( dead, 0.4f );
		Destroy( gameObject, 0.2f );
	}

	public void RecieveEvent ( string eventName ) {
		if( eventName == "shoot" ) {
			Shoot( lastTargetDir );
		}
	}
}
