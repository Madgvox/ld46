﻿using System;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	
	[SerializeField]
	EnemyAI[] prefabs;

	public SpawnMode spawnMode;

	public int spawnCount;

	public float spawnDelayMin;
	public float spawnDelayMax;

	public StartMode startMode = StartMode.Wait;


	int spawned;

	float spawnTimer;
	float nextSpawnTime;

	bool activated;

	int sequencedSpawnIndex;
	RoundSequencer parentSequencer;
	// Use this for initialization
	void Start () {
		
	}

	void SetNextSpawnTime () {
		spawnTimer = 0;
		nextSpawnTime = UnityEngine.Random.Range( spawnDelayMin, spawnDelayMax );
	}
	
	// Update is called once per frame
	void Update () {
		if( activated && spawned < spawnCount ) {
			spawnTimer += Time.deltaTime;

			if( spawnTimer > nextSpawnTime ) {
				Spawn( prefabs, spawnMode );
				spawned += 1;
				SetNextSpawnTime();
			}
		}
	}

	internal void Activate ( RoundAction roundAction, RoundSequencer seq ) {
		if( roundAction.type == ActionType.Spawn ) {
			var prefabArr = prefabs;
			if( roundAction.spawnPrefabOverride.Length > 0 ) prefabArr = roundAction.spawnPrefabOverride;
			var mode = spawnMode;
			if( roundAction.spawnModeOverride != SpawnMode.None ) mode = roundAction.spawnModeOverride;
			Spawn( prefabArr, mode );

		} else if( roundAction.type == ActionType.Activate || roundAction.type == ActionType.ActivateNoReset ) {
			if( roundAction.spawnCountOverride > 0 ) {
				spawnCount = roundAction.spawnCountOverride;
			}

			if( roundAction.spawnDelayMaxOverride > 0 ) {
				spawnDelayMax = roundAction.spawnDelayMaxOverride;
			}

			if( roundAction.spawnDelayMinOverride > 0 ) {
				spawnDelayMin = roundAction.spawnDelayMinOverride;
			}

			if( roundAction.spawnModeOverride != SpawnMode.None ) {
				spawnMode = roundAction.spawnModeOverride;
			}

			if( roundAction.spawnPrefabOverride.Length > 0 ) prefabs = roundAction.spawnPrefabOverride;

			activated = true;

			if( roundAction.type == ActionType.Activate ) {
				spawned = 0;
				spawnTimer = 0;
				SetNextSpawnTime();

				if( startMode == StartMode.Immediate ) {
					nextSpawnTime = 0;
				}
			}
		} else if( roundAction.type == ActionType.Deactivate ) {
			activated = false;
		}

		parentSequencer = seq;
	}

	private void Spawn ( EnemyAI[] prefabs, SpawnMode mode ) {
		EnemyAI prefab;
		if( mode == SpawnMode.Sequence ) {
			sequencedSpawnIndex = sequencedSpawnIndex % prefabs.Length;
			prefab = prefabs[ sequencedSpawnIndex++ ];
		} else {
			prefab = prefabs[ UnityEngine.Random.Range( 0, prefabs.Length ) ];
		}

		var enemy = Instantiate( prefab );
		var part = Instantiate( Resources.Load<ParticleSystem>( "hit_particles" ) );
		part.transform.position = transform.position;
		enemy.transform.position = transform.position;
		parentSequencer.TrackEnemy( enemy );
	}
}
