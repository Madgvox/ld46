﻿using UnityEngine;

public class StickHit : MonoBehaviour {
	[SerializeField]
	SpriteRenderer spriteRenderer;

	public Sprite[] sprites;

	public Collider2D collider;

	public float fadeSpeed;

	private void Awake () {
		var facing = Game.instance.player.fineFacing;

		float rotation;
		if( facing.x > 0 ) {
			if( facing.y == 0 ) {
				rotation = 0;
			} else if( facing.y > 0 ) {
				rotation = 45;
			} else {
				rotation = 360-45;
			}
		} else if( facing.x == 0 ) {
			if( facing.y == 0 ) {
				rotation = 0;
			} else if( facing.y > 0 ) {
				rotation = 90;
			} else {
				rotation = 360-90;
			}
		} else {
			if( facing.y == 0 ) {
				rotation = 180;
			} else if( facing.y > 0 ) {
				rotation = 135;
			} else {
				rotation = 360-135;
			}
		}

		if( facing.y > 0 ) {
			spriteRenderer.sortingOrder = -5;
		}

		if( facing.y < 0 ) {
			spriteRenderer.sortingOrder = 5;
		}

		var r = collider.transform.localEulerAngles;
		r.z = rotation;
		collider.transform.localEulerAngles = r;

		r = spriteRenderer.transform.localEulerAngles;
		r.z = rotation;
		spriteRenderer.transform.localEulerAngles = r;

		spriteRenderer.sprite = sprites[ Random.Range( 0, sprites.Length ) ];
	}

	float fade = 1;
	private void Update () {
		var color = spriteRenderer.color;
		fade -= Time.deltaTime * fadeSpeed;
		color.a = fade;

		spriteRenderer.color = color;

		if( fade < 0 ) {
			Destroy( gameObject );
		}
	}
}
