﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Rounds : ScriptableObject {
	public List<RoundDefinition> rounds;

	internal RoundDefinition GetRoundAndIndexByName ( string name, out int index ) {
		index = rounds.FindIndex( r => r.roundName == name );
		return rounds[ index ];
	}
}
