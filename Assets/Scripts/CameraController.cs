﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public Transform target;

	public Vector2 bounds;

	Vector3 vel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var targetPos = target.position;
		targetPos.z = transform.position.z;

		transform.position = Vector3.SmoothDamp( transform.position, targetPos, ref vel, 0.2f );
	}

	internal void ResetPosition () {
		var targetPos = target.position;
		targetPos.z = transform.position.z;

		transform.position = targetPos;
		vel = Vector3.zero;
	}
}
