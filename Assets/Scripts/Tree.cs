﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tree : Pickable {

	[Header( "Tree" )]
	[SerializeField]
	DistanceLimiter limiter;

	[SerializeField]
	Collider2D collider;

	[SerializeField]
	Hitbox baseHitbox;
	[SerializeField]
	public Hitbox projectileHitbox;

	public AudioClip treeHurt;

	public override int priority {
		get {
			return 1;
		}
	}

	public override PickableType type {
		get {
			return PickableType.Tree;
		}
	}

	public override int sortIndex {
		get {
			throw new System.NotImplementedException();
		}

		set {
			throw new System.NotImplementedException();
		}
	}

	public override void OnEnterPickup ( Player player ) {
		limiter.Disable();
		collider.enabled = false;
		baseHitbox.Disable();
		rigidbody.simulated = false;
		projectileHitbox.Disable();
		Messenger.SendEvent( "Tree::OnEnterPickup" );
	}

	internal void PrepareIntroMode () {
		inOutroMode = false;
		limiter.gameObject.SetActive( false );
		limiter.Disable();
	}

	public override void OnExitPickup () {
		if( !inOutroMode ) {
			limiter.Enable();
			baseHitbox.Enable();
			projectileHitbox.Enable();
		}
		rigidbody.simulated = true;
		collider.enabled = true;
		Messenger.SendEvent( "Tree::OnExitPickup" );
	}

	public override void Use () {
		Debug.Log( "tree cannot be used" );
	}

	public void IntroEnterArea () {
		limiter.gameObject.SetActive( true );
		limiter.Enable();
		collider.enabled = true;
		baseHitbox.Enable();
		projectileHitbox.Enable();
	}

	public void PrepareOutroMode () {
		inOutroMode = true;
		limiter.gameObject.SetActive( false );
		limiter.Disable();
		baseHitbox.Disable();
		projectileHitbox.Disable();
	}

	// Use this for initialization
	void Start () {
		baseHitbox.onHit += OnBaseHit;
		projectileHitbox.onHit += OnTopHit;
	}

	Dictionary<HitSource,float> invulnTimers = new Dictionary<HitSource, float>();
	private bool inOutroMode;

	private void OnBaseHit ( HitSource hit ) {
		TryDamage( hit, Vector2.zero );
		//throw new NotImplementedException();
	}

	protected override void FixedUpdate () {
		// tree has no physics
	}

	private void OnTopHit ( HitSource hit ) {
		TryDamage( hit, Vector2.zero );
		//throw new NotImplementedException();
	}

	public override bool TryDamage ( HitSource obj, Vector2 knockbackForce, bool ignoreInvuln = false ) {
		float lastHitTime;
		bool canHit = ignoreInvuln;

		if( !canHit ) {
			if( invulnTimers.TryGetValue( obj, out lastHitTime ) ) {
				if( lastHitTime < Time.time ) {
					invulnTimers.Remove( obj );
					canHit = true;
				}
			} else {
				canHit = true;
			}
		}

		if( canHit ) {
			invulnTimers.Add( obj, Time.time + invulnDuration );

			knockbackVelocity += knockbackForce;

			Damage( obj.damageAmount );
			PlaySound( treeHurt, 0.4f );
			return true;
		}

		return false;
	}

	protected override void OnKill () {
		// dont destroy on kill
		Debug.Log( "TREE DED" );
		SceneManager.LoadScene( "__intro", LoadSceneMode.Single );
		SceneManager.LoadScene( "Root", LoadSceneMode.Additive );
	}

	// Update is called once per frame
	void Update () {
		if( invulnTimer > 0 ) {
			invulnTimer -= Time.deltaTime;
		}
	}
}
