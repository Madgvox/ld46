﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : PhysicsBase {
	public float moveSpeed = 1f;

	protected int facingX;

	public AudioClip hurt;
	public AudioClip dead;

	[SerializeField]
	protected Animator animator;
	public Transform damagePoint;


	void Start () {
		if( animator == null ) animator = GetComponentInChildren<Animator>();
	}

	// Update is called once per frame
	protected override void OnFixedUpdate () {
		var target = Game.instance.tree.transform;
		if( Game.instance.player.isHoldingTree ) {
			target = Game.instance.player.transform;
		}
		if( target ) {
			Vector2 targetDir = target.position - transform.position;
			targetDir.Normalize();

			var current = animator.GetInteger( "facing_x" );

			facingX = (int)Mathf.Sign( targetDir.x );

			if( current != facingX ) {
				animator.SetTrigger( "face_change" );
			}
			animator.SetInteger( "facing_x", facingX );

			Debug.DrawRay( transform.position, targetDir.normalized );
			velocity = targetDir * moveSpeed;
		}
	}

	public override bool TryDamage ( HitSource hitSource, Vector2 knockbackForce = default( Vector2 ), bool ignoreInvuln = false ) {
		PlaySound( hurt );
		return base.TryDamage( hitSource, knockbackForce, ignoreInvuln );
	}

	protected override void OnKill () {
		PlaySound( dead, 0.4f );
		if( audioSource ) {
			audioSource.transform.parent = null;
			Destroy( audioSource.gameObject, 5f );
		}
		Destroy( gameObject, 0.2f );
	}
}
