﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorBobble : MonoBehaviour {

	Vector3 homePos;

	public float bobbleSpeed = 2;
	public float bobbleMagnitude = 1;

	// Use this for initialization
	void Start () {
		homePos = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		transform.localPosition = homePos + Vector3.up * Mathf.Sin( Time.time * bobbleSpeed ) * bobbleMagnitude;
	}
}
