﻿using UnityEngine;

public class Projectile : PhysicsBase, HitSource {
	public Vector3 position {
		get {
			return transform.position;
		}
	}

	public float damageAmount {
		get {
			return 2;
		}
	}

	public void OnHit () {
		var part = Instantiate( Resources.Load<ParticleSystem>( "hit_particles" ) );
		part.transform.position = GetComponentInChildren<SpriteRenderer>().transform.position;

		Destroy( gameObject );
	}

	void Start () {
		Destroy( gameObject, 20f );
	}

	//private void OnTriggerEnter2D ( UnityEngine.Collider2D collision ) {
	//	Destroy( gameObject );
	//}
}
