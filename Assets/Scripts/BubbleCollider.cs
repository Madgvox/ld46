﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleCollider : MonoBehaviour {

	public float yRadius;
	public float xRadius;

	public int quality = 360;

	// Use this for initialization
	void Start () {
		var coll = gameObject.AddComponent<EdgeCollider2D>();

		var deg = 360f / quality;

		var firstPt = Vector2.zero;

		var pts = new Vector2[ quality + 1 ];
		var angle = deg;
		for( int i = 0; i < quality; i++ ) {
			var x = Mathf.Sin( Mathf.Deg2Rad * angle ) * xRadius;
			var y = Mathf.Cos( Mathf.Deg2Rad * angle ) * yRadius;

			var pt = new Vector2( x, y );
			if( i == 0 ) {
				firstPt = pt;
			}
			pts[ i ] = pt;

			angle += deg;
		}
		pts[ quality ] = firstPt;

		coll.points = pts;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnDrawGizmosSelected () {
		if( !Application.isPlaying ) {
			Gizmos.color = new Color( 1, 1, 1, 0.3f );
			Gizmos.matrix = transform.localToWorldMatrix;

			var deg = 360f / quality;

			var lastPt = Vector2.zero;
			var firstPt = Vector2.zero;

			var angle = deg;
			for( int i = 0; i < quality; i++ ) {
				var x = Mathf.Sin( Mathf.Deg2Rad * angle ) * xRadius;
				var y = Mathf.Cos( Mathf.Deg2Rad * angle ) * yRadius;

				var pt = new Vector2( x, y );
				if( i > 0 ) Gizmos.DrawLine( lastPt, pt );
				lastPt = pt;
				if( i == 0 ) {
					firstPt = pt;
				}

				angle += deg;
			}

			Gizmos.DrawLine( lastPt, firstPt );

			//Gizmos.DrawWireSphere( transform.position, radius );
		}
	}
}
