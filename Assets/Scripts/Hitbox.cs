﻿using System;
using UnityEngine;

public interface HitSource {
	Vector3 position { get; }
	float damageAmount { get; }

	void OnHit ();
}

public class Hitbox : MonoBehaviour, HitSource {

	public Action<HitSource> onHit;


	private bool hitEnabled = true;

	public float hitDamage = 5;

	public Vector3 position {
		get {
			return transform.position;
		}
	}

	public float damageAmount {
		get {
			return hitDamage;
		}
	}

	private void OnTriggerStay2D ( Collider2D collider ) {
		if( hitEnabled ) {
			var hitSource = collider.GetComponent<HitSource>();
			if( hitSource == null ) return;
			if( onHit != null ) onHit( hitSource );
			hitSource.OnHit();
		}
	}

	internal void Enable () {
		hitEnabled = true;
	}

	internal void Disable () {
		hitEnabled = false;
	}

	public void OnHit () {}
}