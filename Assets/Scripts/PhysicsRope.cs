﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class PhysicsRope : MonoBehaviour {

	[SerializeField]
	Rigidbody2D linkPrefab;

	[Space]
	public float length;
	public float ropePhysicsWidth;

	public int numLinks;

	public Rigidbody2D head;
	public Rigidbody2D tail;

	public LineRenderer line;

	List<Rigidbody2D> links = new List<Rigidbody2D>();

	Vector3[] linePositions;

	// Use this for initialization
	void Start () {
		var linkLength = length / numLinks;
		Rigidbody2D lastLink = null;
		linePositions = new Vector3[ numLinks + 2 ];

		line.startWidth = ropePhysicsWidth;
		line.endWidth = ropePhysicsWidth;
		line.positionCount = linePositions.Length;

		for( int i = 0; i < numLinks; i++ ) {
			var link = Instantiate( linkPrefab, transform );
			link.transform.parent = transform;

			var collider = link.GetComponent<CapsuleCollider2D>();
			collider.size = new Vector2( linkLength, ropePhysicsWidth );
			var radiusOffset = linkLength * 0.5f;
			collider.offset = new Vector2( radiusOffset, 0 );

			var joint = link.GetComponent<HingeJoint2D>();
			joint.anchor = new Vector2( radiusOffset * 2, 0 );
			if( i == 0 ) {
				head.GetComponent<HingeJoint2D>().connectedBody = link;
			} else if( i == numLinks - 1 ) {
				link.GetComponent<HingeJoint2D>().connectedBody = tail;
			}

			if( lastLink != null ) {
				lastLink.GetComponent<HingeJoint2D>().connectedBody = link;		
			}

			lastLink = link;
			links.Add( link );
		}
	}

	private void Update () {

		linePositions[ 0 ] = head.position;

		for( int i = 0; i < links.Count; i++ ) {
			linePositions[ i + 1 ] = links[ i ].position;
			//links[ i ].velocity = new Vector2( 0, 0 );
		}

		linePositions[ linePositions.Length - 1 ] = tail.position;

		line.SetPositions( linePositions );
		
		//Debug.DrawLine( head.transform.position, links[ 0 ].transform.position );

		//for( int i = 1; i < links.Count; i++ ) {
		//	Debug.DrawLine( links[ i - 1 ].transform.position, links[ i ].transform.position );
		//}

		//Debug.DrawLine( tail.transform.position, links[ links.Count - 1 ].transform.position );
	}
}
