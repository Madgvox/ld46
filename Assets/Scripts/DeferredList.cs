﻿using System.Collections.Generic;
using System;

public interface IDeferredList<T> : IList<T> {
	bool locked { get; set; }
}

public class DeferredList<T> : List<T>, IDeferredList<T> {
	int lockDepth;
	public bool locked {
		get {
			return lockDepth > 0;
		}
		set {
			if( value ) {
				lockDepth += 1;
			} else {
				lockDepth -= 1;
				if( lockDepth < 0 ) throw new ArgumentException( "Attempted to pop more locks then was pushed.", "locked" );
			}

			if( lockDepth == 0 ) {
				FlushChanges();
			}
		}
	}

	struct ListChange {
		public enum ChangeAction {
			Add,
			Remove,
			Insert,
			RemoveAt
		}

		public ChangeAction action;
		public T item;
		public int index;

		private ListChange ( ChangeAction action, T item ) {
			this.action = action;
			this.item = item;
			this.index = -1;
		}

		private ListChange ( ChangeAction action, int index ) {
			this.action = action;
			this.item = default( T );
			this.index = index;
		}

		private ListChange ( ChangeAction action, int index, T item ) {
			this.action = action;
			this.item = item;
			this.index = index;
		}

		public static ListChange Add ( T item ) {
			return new ListChange( ChangeAction.Add, item );
		}

		public static ListChange Remove ( T item ) {
			return new ListChange( ChangeAction.Remove, item );
		}

		public static ListChange Insert ( int index, T item ) {
			return new ListChange( ChangeAction.Insert, index, item );
		}

		public static ListChange RemoveAt ( int index ) {
			return new ListChange( ChangeAction.RemoveAt, index );
		}
	}

	Queue<ListChange> delayedChanges = new Queue<ListChange>();

	new public void Add ( T item ) {
		if( locked ) {
			delayedChanges.Enqueue( ListChange.Add( item ) );
		} else {
			base.Add( item );
		}
	}

	new public bool Remove ( T item ) {
		if( locked ) {
			delayedChanges.Enqueue( ListChange.Remove( item ) );
			return true;
		} else {
			return base.Remove( item );
		}
	}

	new public void RemoveAt ( int index ) {
		if( locked ) {
			delayedChanges.Enqueue( ListChange.RemoveAt( index ) );
		} else {
			base.RemoveAt( index );
		}
	}

	new public void Insert ( int index, T item ) {
		if( locked ) {
			delayedChanges.Enqueue( ListChange.Insert( index, item ) );
		} else {
			base.Insert( index, item );
		}
	}

	void FlushChanges () {
		while( delayedChanges.Count > 0 ) {
			var d = delayedChanges.Dequeue();
			switch( d.action ) {
				case ListChange.ChangeAction.Add:
					base.Add( d.item );
					break;
				case ListChange.ChangeAction.Remove:
					base.Remove( d.item );
					break;
				case ListChange.ChangeAction.Insert:
					base.Insert( d.index, d.item );
					break;
				case ListChange.ChangeAction.RemoveAt:
					base.RemoveAt( d.index );
					break;
			}
		}
	}
}
