﻿using UnityEngine;

public enum PickableType {
	None,
	Tree,
	Weapon
}

public abstract class Pickable : PhysicsBase {
	public abstract int priority { get; }
	public abstract PickableType type { get; }
	public abstract int sortIndex { get; set; }

	public abstract void OnEnterPickup ( Player player );
	public abstract void OnExitPickup ();
	public abstract void Use ();
}
