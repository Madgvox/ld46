﻿using System;
using System.Collections.Generic;

public delegate void Callback ();
public delegate void Callback<T0> ( T0 arg0 );
public delegate void Callback<T0, T1> ( T0 arg0, T1 arg1 );
public delegate void Callback<T0, T1, T2> ( T0 arg0, T1 arg1, T2 arg2 );

public class MessageSignatureCompararer : IEqualityComparer<MessageSignature> {
	public bool Equals ( MessageSignature x, MessageSignature y ) {
		return x.Equals( y );
	}

	public int GetHashCode ( MessageSignature obj ) {
		return obj.GetHashCode();
	}
}

public interface MessageSignature {
	bool Equals ( MessageSignature other );
	int GetHashCode ();
}

public struct MessageSignature<TAction> : MessageSignature, IEquatable<MessageSignature> {
	static EqualityComparer<TAction> actionComparer = EqualityComparer<TAction>.Default;

	public readonly TAction action;

	static readonly Type actionType = typeof( TAction );

	readonly int hash;

	public MessageSignature ( TAction action ) {
		this.action = action;

		hash = MakeHashCode( action );
	}

	public override bool Equals ( object obj ) {
		if( !( obj is MessageSignature<TAction> ) ) return false;
		var msg = (MessageSignature<TAction>)obj;
		return actionComparer.Equals( action, msg.action );
	}

	public bool Equals ( MessageSignature other ) {
		if( !( other is MessageSignature<TAction> ) ) return false;
		var msg = (MessageSignature<TAction>)other;
		return actionComparer.Equals( action, msg.action );
	}

	public override int GetHashCode () {
		return hash;
	}



	static int MakeHashCode ( TAction action ) {
		if( actionType.IsValueType || action != null ) {
			return action.GetHashCode();
		} else {
			return -1;
		}
	}
}

public struct MessageSignature<TAction, T0> : MessageSignature, IEquatable<MessageSignature> {
	static EqualityComparer<TAction> actionComparer = EqualityComparer<TAction>.Default;

	public readonly TAction action;

	static readonly Type actionType = typeof( TAction );

	readonly int hash;


	public MessageSignature ( TAction action ) {
		this.action = action;

		hash = MakeHashCode( action );
	}

	public override bool Equals ( object obj ) {
		if( !( obj is MessageSignature<TAction, T0> ) ) return false;
		var msg = (MessageSignature<TAction, T0>)obj;
		return actionComparer.Equals( action, msg.action );
	}

	public bool Equals ( MessageSignature other ) {
		if( !( other is MessageSignature<TAction, T0> ) ) return false;
		var msg = (MessageSignature<TAction, T0>)other;
		return actionComparer.Equals( action, msg.action );
	}

	public override int GetHashCode () {
		return hash;
	}



	static readonly int t0Hash = typeof( T0 ).GetHashCode();
	static int MakeHashCode ( TAction action ) {
		int hash = 17;
		if( actionType.IsValueType || action != null ) {
			hash = hash * 23 + action.GetHashCode();
		}
		hash = hash * 23 + t0Hash;
		return hash;
	}
}

public struct MessageSignature<TAction, T0, T1> : MessageSignature, IEquatable<MessageSignature> {
	static EqualityComparer<TAction> actionComparer = EqualityComparer<TAction>.Default;

	public readonly TAction action;

	static readonly Type actionType = typeof( TAction );

	readonly int hash;

	public MessageSignature ( TAction action ) {
		this.action = action;

		hash = MakeHashCode( action );
	}

	public override bool Equals ( object obj ) {
		if( !( obj is MessageSignature<TAction, T0, T1> ) ) return false;
		var msg = (MessageSignature<TAction, T0, T1>)obj;
		return actionComparer.Equals( action, msg.action );
	}

	public bool Equals ( MessageSignature other ) {
		if( !( other is MessageSignature<TAction, T0, T1> ) ) return false;
		var msg = (MessageSignature<TAction, T0, T1>)other;
		return actionComparer.Equals( action, msg.action );
	}

	public override int GetHashCode () {
		return hash;
	}



	static readonly int t0Hash = typeof( T0 ).GetHashCode();
	static readonly int t1Hash = typeof( T1 ).GetHashCode();
	static int MakeHashCode ( TAction action ) {
		int hash = 17;
		if( actionType.IsValueType || action != null ) {
			hash = hash * 23 + action.GetHashCode();
		}
		hash = hash * 23 + t0Hash;
		hash = hash * 23 + t1Hash;
		return hash;
	}
}

public struct MessageSignature<TAction, T0, T1, T2> : MessageSignature, IEquatable<MessageSignature> {
	static EqualityComparer<TAction> actionComparer = EqualityComparer<TAction>.Default;

	public readonly TAction action;

	static readonly Type actionType = typeof( TAction );

	readonly int hash;

	public MessageSignature ( TAction action ) {
		this.action = action;

		hash = MakeHashCode( action );
	}

	public override bool Equals ( object obj ) {
		if( !( obj is MessageSignature<TAction, T0, T1, T2> ) ) return false;
		var msg = (MessageSignature<TAction, T0, T1, T2>)obj;
		return actionComparer.Equals( action, msg.action );
	}

	public bool Equals ( MessageSignature other ) {
		if( !( other is MessageSignature<TAction, T0, T1, T2> ) ) return false;
		var msg = (MessageSignature<TAction, T0, T1, T2>)other;
		return actionComparer.Equals( action, msg.action );
	}

	public override int GetHashCode () {
		return hash;
	}



	static readonly int t0Hash = typeof( T0 ).GetHashCode();
	static readonly int t1Hash = typeof( T1 ).GetHashCode();
	static readonly int t2Hash = typeof( T2 ).GetHashCode();
	static int MakeHashCode ( TAction action ) {
		int hash = 17;
		if( actionType.IsValueType || action != null ) {
			hash = hash * 23 + action.GetHashCode();
		}
		hash = hash * 23 + t0Hash;
		hash = hash * 23 + t1Hash;
		hash = hash * 23 + t2Hash;
		return hash;
	}
}

public class EventList : DeferredList<Delegate> { }

public partial class Messenger {
	public static Messenger global = new Messenger();
	static MessageSignatureCompararer comparer = new MessageSignatureCompararer();

	public static MessageSignature CreateMessageSignature<TAction> ( TAction actionType ) {
		return new MessageSignature<TAction>( actionType );
	}

	public static MessageSignature CreateMessageSignature<TAction, T0> ( TAction actionType ) {
		return new MessageSignature<TAction, T0>( actionType );
	}

	public static MessageSignature CreateMessageSignature<TAction, T0, T1> ( TAction actionType ) {
		return new MessageSignature<TAction, T0, T1>( actionType );
	}

	public static MessageSignature CreateMessageSignature<TAction, T0, T1, T2> ( TAction actionType ) {
		return new MessageSignature<TAction, T0, T1, T2>( actionType );
	}



	public static MessageSignature Subscribe<TAction> ( TAction actionType, Callback callback ) {
		return global.AddReciever( actionType, callback );
	}

	public static MessageSignature Subscribe<TAction, T0> ( TAction actionType, Callback<T0> callback ) {
		return global.AddReciever( actionType, callback );
	}

	public static MessageSignature Subscribe<TAction, T0, T1> ( TAction actionType, Callback<T0, T1> callback ) {
		return global.AddReciever( actionType, callback );
	}

	public static MessageSignature Subscribe<TAction, T0, T1, T2> ( TAction actionType, Callback<T0, T1, T2> callback ) {
		return global.AddReciever( actionType, callback );
	}

	public static void Subscribe ( MessageSignature signature, Callback callback ) { Subscribe( signature, (Delegate)callback ); }
	public static void Subscribe<T0> ( MessageSignature signature, Callback<T0> callback ) { Subscribe( signature, (Delegate)callback ); }
	public static void Subscribe<T0, T1> ( MessageSignature signature, Callback<T0, T1> callback ) { Subscribe( signature, (Delegate)callback ); }
	public static void Subscribe<T0, T1, T2> ( MessageSignature signature, Callback<T0, T1, T2> callback ) { Subscribe( signature, (Delegate)callback ); }

	public static void Subscribe ( MessageSignature signature, Delegate callback ) {
		global.AddReciever( signature, callback );
	}


	public static void Unsubscribe<TAction> ( TAction actionType, Callback callback ) {
		global.RemoveReciever( actionType, callback );
	}

	public static void Unsubscribe<TAction, T0> ( TAction actionType, Callback<T0> callback ) {
		global.RemoveReciever( actionType, callback );
	}

	public static void Unsubscribe<TAction, T0, T1> ( TAction actionType, Callback<T0, T1> callback ) {
		global.RemoveReciever( actionType, callback );
	}

	public static void Unsubscribe<TAction, T0, T1, T2> ( TAction actionType, Callback<T0, T1, T2> callback ) {
		global.RemoveReciever( actionType, callback );
	}

	public static void Unsubscribe ( MessageSignature signature, Callback callback ) { Unsubscribe( signature, (Delegate)callback ); }
	public static void Unsubscribe<T0> ( MessageSignature signature, Callback<T0> callback ) { Unsubscribe( signature, (Delegate)callback ); }
	public static void Unsubscribe<T0, T1> ( MessageSignature signature, Callback<T0, T1> callback ) { Unsubscribe( signature, (Delegate)callback ); }
	public static void Unsubscribe<T0, T1, T2> ( MessageSignature signature, Callback<T0, T1, T2> callback ) { Unsubscribe( signature, (Delegate)callback ); }

	static void Unsubscribe ( MessageSignature signature, Delegate callback ) {
		global.RemoveReciever( signature, callback );
	}

	public static void SendEvent<TAction> ( TAction actionType ) {
		global.SendMessage( actionType );
	}

	public static void SendEvent<TAction, T0> ( TAction actionType, T0 arg0 ) {
		global.SendMessage( actionType, arg0 );
	}

	public static void SendEvent<TAction, T0, T1> ( TAction actionType, T0 arg0, T1 arg1 ) {
		global.SendMessage( actionType, arg0, arg1 );
	}

	public static void SendEvent<TAction, T0, T1, T2> ( TAction actionType, T0 arg0, T1 arg1, T2 arg2 ) {
		global.SendMessage( actionType, arg0, arg1, arg2 );
	}

	public static void SendEvent ( MessageSignature signature ) {
		global.SendMessage( signature );
	}

	public static void SendEvent<T0> ( MessageSignature signature, T0 arg0 ) {
		global.SendMessage( signature, arg0 );
	}

	public static void SendEvent<T0, T1> ( MessageSignature signature, T0 arg0, T1 arg1 ) {
		global.SendMessage( signature, arg0, arg1 );
	}

	public static void SendEvent<T0, T1, T2> ( MessageSignature signature, T0 arg0, T1 arg1, T2 arg2 ) {
		global.SendMessage( signature, arg0, arg1, arg2 );
	}
}



public partial class Messenger {
	Dictionary<MessageSignature, EventList> recievers = new Dictionary<MessageSignature, EventList>( comparer );

	public MessageSignature AddReciever<TAction> ( TAction actionType, Callback callback ) {
		var signature = (MessageSignature)new MessageSignature<TAction>( actionType );
		AddReciever( signature, (Delegate)callback );
		return signature;
	}

	public MessageSignature AddReciever<TAction, T0> ( TAction actionType, Callback<T0> callback ) {
		var signature = (MessageSignature)new MessageSignature<TAction, T0>( actionType );
		AddReciever( signature, (Delegate)callback );
		return signature;
	}

	public MessageSignature AddReciever<TAction, T0, T1> ( TAction actionType, Callback<T0, T1> callback ) {
		var signature = (MessageSignature)new MessageSignature<TAction, T0, T1>( actionType );
		AddReciever( signature, (Delegate)callback );
		return signature;
	}

	public MessageSignature AddReciever<TAction, T0, T1, T2> ( TAction actionType, Callback<T0, T1, T2> callback ) {
		var signature = (MessageSignature)new MessageSignature<TAction, T0, T1, T2>( actionType );
		AddReciever( signature, (Delegate)callback );
		return signature;
	}

	public void AddReciever ( MessageSignature signature, Callback callback ) { AddReciever( signature, (Delegate)callback ); }
	public void AddReciever<T0> ( MessageSignature signature, Callback<T0> callback ) { AddReciever( signature, (Delegate)callback ); }
	public void AddReciever<T0, T1> ( MessageSignature signature, Callback<T0, T1> callback ) { AddReciever( signature, (Delegate)callback ); }
	public void AddReciever<T0, T1, T2> ( MessageSignature signature, Callback<T0, T1, T2> callback ) { AddReciever( signature, (Delegate)callback ); }

	public void AddReciever ( MessageSignature signature, Delegate callback ) {
		EventList actions;
		if( !recievers.TryGetValue( signature, out actions ) ) {
			actions = new EventList();
			recievers.Add( signature, actions );
		}

		actions.Add( callback );
	}


	public void RemoveReciever<TAction> ( TAction actionType, Callback callback ) {
		var def = new MessageSignature<TAction>( actionType );
		RemoveReciever( def, (Delegate)callback );
	}

	public void RemoveReciever<TAction, T0> ( TAction actionType, Callback<T0> callback ) {
		var def = new MessageSignature<TAction, T0>( actionType );
		RemoveReciever( def, (Delegate)callback );
	}

	public void RemoveReciever<TAction, T0, T1> ( TAction actionType, Callback<T0, T1> callback ) {
		var def = new MessageSignature<TAction, T0, T1>( actionType );
		RemoveReciever( def, (Delegate)callback );
	}

	public void RemoveReciever<TAction, T0, T1, T2> ( TAction actionType, Callback<T0, T1, T2> callback ) {
		var def = new MessageSignature<TAction, T0, T1, T2>( actionType );
		RemoveReciever( def, (Delegate)callback );
	}

	public void RemoveReciever ( MessageSignature signature, Callback callback ) { RemoveReciever( signature, (Delegate)callback ); }
	public void RemoveReciever<T0> ( MessageSignature signature, Callback<T0> callback ) { RemoveReciever( signature, (Delegate)callback ); }
	public void RemoveReciever<T0, T1> ( MessageSignature signature, Callback<T0, T1> callback ) { RemoveReciever( signature, (Delegate)callback ); }
	public void RemoveReciever<T0, T1, T2> ( MessageSignature signature, Callback<T0, T1, T2> callback ) { RemoveReciever( signature, (Delegate)callback ); }

	void RemoveReciever ( MessageSignature signature, Delegate callback ) {
		EventList actions;
		if( recievers.TryGetValue( signature, out actions ) ) {
			actions.Remove( callback );
		}
	}

	public void SendMessage<TAction> ( TAction actionType ) {
		var def = new MessageSignature<TAction>( actionType );
		SendMessage( (MessageSignature)def );
	}

	public void SendMessage<TAction, T0> ( TAction actionType, T0 arg0 ) {
		var def = new MessageSignature<TAction, T0>( actionType );
		SendMessage( (MessageSignature)def, arg0 );
	}

	public void SendMessage<TAction, T0, T1> ( TAction actionType, T0 arg0, T1 arg1 ) {
		var def = new MessageSignature<TAction, T0, T1>( actionType );
		SendMessage( (MessageSignature)def, arg0, arg1 );
	}

	public void SendMessage<TAction, T0, T1, T2> ( TAction actionType, T0 arg0, T1 arg1, T2 arg2 ) {
		var def = new MessageSignature<TAction, T0, T1, T2>( actionType );
		SendMessage( (MessageSignature)def, arg0, arg1, arg2 );
	}

	public void SendMessage ( MessageSignature signature ) {
		EventList actions;
		if( recievers.TryGetValue( signature, out actions ) ) {
			// kill any events that call themselves
			if( actions.locked ) return;

			actions.locked = true;
			foreach( var action in actions ) {
				( (Callback)action ).Invoke();
			}
			actions.locked = false;
		}
	}

	public void SendMessage<T0> ( MessageSignature signature, T0 arg0 ) {
		EventList actions;
		if( recievers.TryGetValue( signature, out actions ) ) {
			// kill any events that call themselves
			if( actions.locked ) return;

			actions.locked = true;
			foreach( var action in actions ) {
				( (Callback<T0>)action ).Invoke( arg0 );
			}
			actions.locked = false;
		}
	}

	public void SendMessage<T0, T1> ( MessageSignature signature, T0 arg0, T1 arg1 ) {
		EventList actions;
		if( recievers.TryGetValue( signature, out actions ) ) {
			// kill any events that call themselves
			if( actions.locked ) return;

			actions.locked = true;
			foreach( var action in actions ) {
				( (Callback<T0, T1>)action ).Invoke( arg0, arg1 );
			}
			actions.locked = false;
		}
	}

	public void SendMessage<T0, T1, T2> ( MessageSignature signature, T0 arg0, T1 arg1, T2 arg2 ) {
		EventList actions;
		if( recievers.TryGetValue( signature, out actions ) ) {
			// kill any events that call themselves
			if( actions.locked ) return;

			actions.locked = true;
			foreach( var action in actions ) {
				( (Callback<T0, T1, T2>)action ).Invoke( arg0, arg1, arg2 );
			}
			actions.locked = false;
		}
	}

	public void Clear () {
		recievers.Clear();
	}
}
