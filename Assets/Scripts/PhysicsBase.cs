﻿using System;
using UnityEngine;

public abstract class PhysicsBase : MonoBehaviour {
	public Action<PhysicsBase> onDestroy;

	[Header( "Physics" )]
	[SerializeField]
	protected Rigidbody2D rigidbody;

	[SerializeField]
	protected HitFlash flasher;

	public float invulnDuration = 0.5f;
	public float knockbackDecay = 10;

	public float maxHealth;
	[NonSerialized]
	public float health;

	[NonSerialized]
	public Vector2 velocity;
	[NonSerialized]
	public Vector2 knockbackVelocity;

	[SerializeField]
	protected AudioSource audioSource;

	bool alive = true;

	public void PlaySound ( AudioClip clip, float volume = 1 ) {
		if( clip == null ) return;
		audioSource.pitch = UnityEngine.Random.Range( 0.95f, 1.05f );
		audioSource.volume = volume;
		audioSource.PlayOneShot( clip );
	}

	protected float invulnTimer;
	protected virtual void Awake () {
		if( rigidbody == null ) rigidbody = GetComponent<Rigidbody2D>();
		rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;

		if( flasher == null ) flasher = GetComponentInChildren<HitFlash>();

		if( audioSource == null ) audioSource = GetComponentInChildren<AudioSource>();

		health = maxHealth;
	}

	public virtual void Knockback ( Vector2 mag ) {
		knockbackVelocity += mag;
		Debug.Log( knockbackVelocity );
	}

	public void Damage ( float amount ) {
		health -= amount;
		flasher.Hit( 1 );

		if( health <= 0 && alive ) {
			Kill();
		}
	}

	public virtual bool TryDamage ( HitSource hitSource, Vector2 knockbackForce = default(Vector2), bool ignoreInvuln = false ) {
		if( invulnTimer <= 0 || ignoreInvuln ) {
			invulnTimer = invulnDuration;

			knockbackVelocity += knockbackForce;
			Damage( hitSource.damageAmount );
			return true;
		}

		return false;
	}

	public void Kill () {
		alive = false;
		if( onDestroy != null ) onDestroy( this );
		OnKill();
	}

	protected virtual void Update () {
		if( invulnTimer > 0 ) invulnTimer -= Time.deltaTime;
	}

	protected virtual void OnKill () {
		if( audioSource ) {
			audioSource.transform.parent = null;
			Destroy( audioSource.gameObject, 5f );
		}
		Destroy( gameObject );
	}

	protected virtual void FixedUpdate () {
		OnFixedUpdate();

		var vel = velocity;
		vel += knockbackVelocity;
		var mag = knockbackVelocity.magnitude;
		//Debug.Log( this + " " + mag );
		var m2 = mag;
		mag -= knockbackDecay * Time.fixedDeltaTime;
		if( mag < 0 ) mag = 0;
		knockbackVelocity = knockbackVelocity.normalized * mag;


		rigidbody.MovePosition( rigidbody.position + vel * Time.fixedDeltaTime );
	}

	protected virtual void OnFixedUpdate () {}
}
