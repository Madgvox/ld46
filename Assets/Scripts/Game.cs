﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[DefaultExecutionOrder(-10000)]
public class Game : MonoBehaviour {
	public GameUI ui;
	public Player player;
	public Tree tree;
	public CameraController camera;
	public RoundSequencer currentSequence;

	public Rounds rounds;

	public Scene currentSceneContainer;
	public Scene currentSceneBackground;

	public static Coroutine WaitForSeconds ( float seconds, Action action ) {
		return instance.StartCoroutine( Wait( seconds, action ) );
	}

	public static Coroutine WaitForNextFrame ( Action action ) {
		return instance.StartCoroutine( WaitNextFrame( action ) );
	}

	private static Coroutine WaitForEndOfFrame ( Action action ) {
		return instance.StartCoroutine( WaitEndOfFrame( action ) );
	}

	static IEnumerator Wait ( float seconds, Action action ) {
		yield return new WaitForSeconds( seconds );
		action();
	}

	static IEnumerator WaitNextFrame ( Action action ) {
		yield return null;
		action();
	}

	static IEnumerator WaitEndOfFrame ( Action action ) {
		yield return new WaitForEndOfFrame();
		action();
	}

	internal void IntroCutscene () {
		tree.IntroEnterArea();

		inCutscene = true;
		// do cutscene stuff

		WaitForSeconds( 0.4f, () => {
			inCutscene = false;
			Callback onEnterPickup = null;
			onEnterPickup = () => {
				Messenger.Unsubscribe( "Tree::OnEnterPickup", onEnterPickup );
				inCutscene = true;
				WaitForSeconds( 0.5f, () => {
					inCutscene = false;
					currentRoundIndex = -1; // so that it's 0 in EndRound
					EndRound();
				} );
			};
			Messenger.Subscribe( "Tree::OnEnterPickup", onEnterPickup );
		} );
	}

	internal void OutroCutscene () {
		inCutscene = true;

		WaitForSeconds( 2, () => {
			player.PlaceTree();

			WaitForSeconds( 2, () => {
				inCutscene = false;

				tree.PrepareOutroMode();

				camera.target = player.transform;
			} );
		} );
	}

	public void ReloadRound () {
		var enemies = FindObjectsOfType<EnemyAI>();

		foreach( var enemy in enemies ) {
			Destroy( enemy.gameObject );
		}
		LoadRound( currentSceneContainer.name, currentSceneBackground.name, true );
	}

	public bool inCutscene;
	public static Game instance;

	int currentRoundIndex;

	private void Awake () {
		instance = this;

		player = GameObject.FindWithTag( "Player" ).GetComponent<Player>();
		tree = GameObject.FindWithTag( "Tree" ).GetComponent<Tree>();
		camera = GameObject.FindWithTag( "MainCamera" ).GetComponent<CameraController>();

		var containsIntro = false;
		for( var i = 0; i < SceneManager.sceneCount; i++ ) {
			var scene = SceneManager.GetSceneAt( i );

			if( scene.name.StartsWith( "Round" ) ) {
				currentSceneContainer = scene;
			}
			if( scene.name.StartsWith( "Room" ) ) {
				currentSceneBackground = scene;
			}
			if( scene.name == "__intro" ) {
				containsIntro = true;
			}
		}

		containsIntro = true;

		if( containsIntro ) WaitForNextFrame( () => LoadRound( "__intro" ) );
		else {
			int index;
			var currentDef = rounds.GetRoundAndIndexByName( currentSceneContainer.name, out index );

			if( currentDef != null ) {
				currentRoundIndex = index;
			}
			WaitForNextFrame( () => InitRoundScene( currentSceneContainer ) );
		}
	}

	void Update () {
		ui.UpdateGameData( this );
	}

	public void LoadRound ( string roundName, string backgroundName = null, bool forceReload = false ) {
		StartCoroutine( LoadLevels( roundName, backgroundName, forceReload ) );
	}

	IEnumerator LoadLevels ( string roundName, string backgroundName = null, bool forceReload = false ) {
		Debug.Log( "Loading level:" + " " + roundName + " " + backgroundName );

		var backgroundAlreadyLoaded = false;
		var useSpawnPoint = true;
		if( currentSceneBackground.name == backgroundName || backgroundName == null ) {
			backgroundAlreadyLoaded = true;
		}

		if( currentSceneBackground.name == backgroundName ) {
			useSpawnPoint = false;
		}

		AsyncOperation bgOp = null;
		if( !backgroundAlreadyLoaded ) {
			bgOp = SceneManager.LoadSceneAsync( backgroundName, LoadSceneMode.Additive );
			bgOp.allowSceneActivation = false;
		}
		var fgOp = SceneManager.LoadSceneAsync( roundName, LoadSceneMode.Additive );
		fgOp.allowSceneActivation = false;

		if( !backgroundAlreadyLoaded || roundName == "__outro" || forceReload ) {
			ui.wipe.WipeStart();

			yield return new WaitForSeconds( 0.2f );
		}

		while( true ) {
			if( ( backgroundAlreadyLoaded || bgOp.progress >= 0.9f ) && fgOp.progress >= 0.9f ) {
				if( !backgroundAlreadyLoaded ) bgOp.allowSceneActivation = true;
				fgOp.allowSceneActivation = true;

				fgOp.completed += _ => {
					WaitForSeconds( 0.25f, () => {
						ui.wipe.WipeEnd();
					});
					var scene = SceneManager.GetSceneByName( roundName );
					InitRoundScene( scene, useSpawnPoint || forceReload );

					if( currentSceneContainer.IsValid() ) {
						SceneManager.UnloadSceneAsync( currentSceneContainer );
					}
					currentSceneContainer = scene;
				};

				if( !backgroundAlreadyLoaded ) {
					bgOp.completed += _ => {
						var bg = SceneManager.GetSceneByName( backgroundName );

						if( currentSceneBackground.IsValid() ) {
							SceneManager.UnloadSceneAsync( currentSceneBackground );
						}
						currentSceneBackground = bg;
					};
				} else if( backgroundName == null ) {
					if( currentSceneBackground.IsValid() ) {
						SceneManager.UnloadSceneAsync( currentSceneBackground );
					}
					currentSceneBackground = default( Scene );
				}

				break;
			}

			yield return null;
		}
	}

	private void InitRoundScene ( Scene scene, bool useSpawnPoint = true ) {
		if( !scene.IsValid() ) return;
		if( !scene.isLoaded ) return;

		var roots = scene.GetRootGameObjects();
		Debug.Log( roots.Length );
		foreach( var r in roots ) {
			Debug.Log( r );
		}
		if( useSpawnPoint || scene.name == "__intro" ) {
			if( scene.name == "__intro" ) {
				tree.PrepareIntroMode();
				player.PrepareIntroMode();
			}

			var spawn = Array.Find( roots, r => r.GetComponent<SpawnPoint>() != null )?.GetComponent<SpawnPoint>();
			if( spawn != null ) {
				if( !player.isHoldingTree && scene.name != "__intro" ) player.GrabTree( tree );

				player.transform.position = spawn.transform.position;
				//tree.transform.position = spawn.transform.position;

				player.InitHistoryForSpawn();

				if( player.isHoldingItem ) {
					var item = player.item;
					player.DropHeldItem();
					item.Kill();
				}
			}

			camera.ResetPosition();
		}
		var seq = Array.Find( roots, r => r.GetComponent<RoundSequencer>() != null )?.GetComponent<RoundSequencer>();
		if( seq != null ) {
			StartRound( seq );
		} else {
			Debug.Log( "Not starting round, no sequencer!" );
		}

		if( scene.name == "__outro" ) {
			player.PrepareOutroMode();
			//tree.PrepareOutroMode();

			camera.target = player.transform;
			camera.ResetPosition();

			OutroCutscene();
		}
	}

	void StartRound ( RoundSequencer seq ) {
		currentSequence = seq;
		currentSequence.active = true;
	}

	internal void EndRound () {
		if( currentSequence ) {
			currentSequence.active = false;
			currentSequence.Cleanup();

			currentSequence = null;
		}

		currentRoundIndex += 1;

		if( currentRoundIndex >= rounds.rounds.Count ) {
			LoadRound( "__outro" );
		} else {
			var roundDef = rounds.rounds[ currentRoundIndex ];
			LoadRound( roundDef.roundName, roundDef.bgName );
		}
	}
}
