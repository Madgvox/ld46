﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

enum PlayerHoldingState {
	Empty,
	HoldingTree,
	HoldingPickable
}

[SelectionBase]
public class Player : PhysicsBase {

	[SerializeField]
	Hitbox hitbox;
	[SerializeField]
	Hitbox projectileHitbox;

	[SerializeField]
	Stick stick;

	[SerializeField]
	PlayerAnimator animator;

	[SerializeField]
	Transform treeContainer;

	[Space]
	public float moveSpeed = 5;
	public Vector2 treePlaceOffset;
	public Vector2 pickupOffsetH;
	public Vector2 pickupOffsetV;
	public Vector2 pickupSize;
	public LayerMask pickableLayers;
	public Vector2 treeHoldPosition;
	public Vector2 itemHoldPosition;


	public AudioClip hurt;
	public AudioClip hurtInvuln;
	public AudioClip swing;
	public AudioClip pickupTree;
	public AudioClip putdownTree;


	[NonSerialized]
	public Vector2 roughFacing = Vector2.right;
	[NonSerialized]
	public Vector2 fineFacing = Vector2.right;

	[NonSerialized]
	public bool isHoldingTree;
	[NonSerialized]
	public bool isHoldingItem;

	internal void PrepareIntroMode () {
		inIntroMode = true;
	}

	internal void PrepareOutroMode () {
		inOutroMode = true;
		velocity = Vector3.zero;
	}

	//PlayerHoldingState holdingState = PlayerHoldingState.Empty;
	[NonSerialized]
	public Pickable item;

	Vector3[] positionHistory;

	Vector3 lastPosition;

	int positionHistoryIndex;

	[NonSerialized]
	public bool inIntroMode;
	[NonSerialized]
	public bool inOutroMode;


	Tree tree {
		get {
			return Game.instance.tree;
		}
	}

	// Use this for initialization
	void Start () {
		positionHistory = new Vector3[ 100 ];
		positionHistory[ positionHistoryIndex ] = transform.position;
		lastPosition = transform.position;

		hitbox.onHit += OnHit;
		projectileHitbox.onHit += OnHit;

		//if( !inIntroMode ) camera.target = Game.instance.tree.transform;
	}

	protected override void OnKill () {
		SceneManager.LoadScene( "__intro", LoadSceneMode.Single );
		SceneManager.LoadScene( "Root", LoadSceneMode.Additive );
	}

	private void OnHit ( HitSource hit ) {
		var dir = transform.position - hit.position;
		TryDamage( hit, dir.normalized * 7 );
	}

	public override bool TryDamage ( HitSource hitSource, Vector2 knockbackForce = default( Vector2 ), bool ignoreInvuln = false ) {
		if( invulnTimer <= 0 || ignoreInvuln ) {
			invulnTimer = invulnDuration;

			knockbackVelocity += knockbackForce;
			if( isHoldingTree ) {
				PlaySound( hurt );
				Damage( hitSource.damageAmount );
				return true;
			} else {
				PlaySound( hurt );
				flasher.Hit( 1 );
				return false;
			}
		}

		return false;
	}

	// Update is called once per frame
	protected override void Update () {
		base.Update();

		
		var h = Input.GetAxisRaw( "Horizontal" );
		var v = Input.GetAxisRaw( "Vertical" );

		var hv = new Vector2( h, v );
		hv.Normalize();

		hv *= moveSpeed;

		if( Game.instance.inCutscene ) hv = Vector2.zero;

		velocity = hv;

		if( hv != Vector2.zero ) {
			if( velocity.x > 0 ) {
				roughFacing.x = 1;
				fineFacing.x = 1;
			} else if( velocity.x < 0 ) {
				roughFacing.x = -1;
				fineFacing.x = -1;
			} else {
				fineFacing.x = 0;
			}

			if( velocity.y > 0 ) {
				roughFacing.y = 1;
				fineFacing.y = 1;
			} else if( velocity.y < 0 ) {
				roughFacing.y = -1;
				fineFacing.y = -1;
			} else {
				fineFacing.y = 0;
			}
		}

		//Debug.DrawRay( transform.position, roughFacing );
		//Debug.DrawRay( transform.position, fineFacing );

		if( Input.GetButton( "Use" ) && !Game.instance.inCutscene && !inOutroMode ) {
			if( isHoldingItem && !isHoldingTree ) {
				TryUseHeldItem();
			}

			if( !isHoldingItem && !isHoldingTree ) {
				stick.Use();
				PlaySound( swing );
			}
		}

		if( Input.GetButtonDown( "PickupDrop" ) && !Game.instance.inCutscene && !inOutroMode ) {
			var xOffset = pickupOffsetH * roughFacing;
			var yOffset = pickupOffsetV * roughFacing;

			//Debug.DrawLine( transform.position, (Vector2)transform.position + xOffset );
			//Debug.DrawLine( transform.position, (Vector2)transform.position + yOffset );

			Pickable pickable = null;
			CheckPickable( xOffset, ref pickable );
			CheckPickable( yOffset, ref pickable );
			CheckPickable( Vector2.zero, ref pickable );

			if( pickable != null ) {
				if( pickable.type == PickableType.Tree ) {
					GrabTree( (Tree)pickable );
				} else if( !isHoldingTree ) {
					GrabPickable( pickable );
				} else {
					PlaceTree();
				}
			} else {
				if( isHoldingTree ) {
					PlaceTree();
				} else if( isHoldingItem ) {
					DropHeldItem();
				}
			}
		}

		UpdateHeldPosition();

		if( !Game.instance.inCutscene ) {
			if( lastPosition != transform.position ) {
				positionHistoryIndex += 1;
				if( positionHistoryIndex >= positionHistory.Length ) positionHistoryIndex = 0;
				positionHistory[ positionHistoryIndex ] = lastPosition;
			}

			lastPosition = transform.position;
		}

		animator.SetFacing( roughFacing );

		//foreach( var p2 in positionHistory ) {
		//	Debug.DrawLine( p2 - Vector3.right * 0.2f, p2 + Vector3.right * 0.2f, Color.red, 5 );
		//	Debug.DrawLine( p2 - Vector3.up * 0.2f, p2 + Vector3.up * 0.2f, Color.red, 5 );
		//}

		//Debug.DrawLine( positionHistory[ positionHistoryIndex ] - Vector3.right * 0.2f, positionHistory[ positionHistoryIndex ] + Vector3.right * 0.2f, Color.red, 5 );
		//Debug.DrawLine( positionHistory[ positionHistoryIndex ] - Vector3.up * 0.2f, positionHistory[ positionHistoryIndex ] + Vector3.up * 0.2f, Color.red, 5 );
	}

	private void UpdateHeldPosition () {
		//if( isHoldingTree ) {
		//	var pos = treeHoldPosition;
		//	pos.x *= roughFacing.x;
		//	tree.transform.localPosition = pos;
		//}

		if( isHoldingItem ) {
			var pos = itemHoldPosition;
			pos.x *= roughFacing.x;
			item.transform.localPosition = pos;
			item.sortIndex = -(int)roughFacing.y;
		}
	}

	private Vector3 WalkHistoryToDist ( Vector3 compare, Vector2 extents, bool resetInd = true ) {
		var ind = positionHistoryIndex;
		var c = 0;

		var pos = positionHistory[ ind ];
		while( c < positionHistory.Length ) {
			pos = positionHistory[ ind ];
			Debug.Log( pos );

			Debug.DrawLine( pos - Vector3.right * 0.2f, pos + Vector3.right * 0.2f, Color.red, 5 );
			Debug.DrawLine( pos - Vector3.up * 0.2f, pos + Vector3.up * 0.2f, Color.red, 5 );

			if( Math.Abs( pos.x - compare.x ) > extents.x
				|| Math.Abs( pos.y - compare.y ) > extents.y ) {

				positionHistoryIndex = ind;
				return pos;
			}
			c += 1;
			ind -= 1;
			if( ind < 0 ) {
				ind = positionHistory.Length - 1;
			}
		}

		return pos;
	}

	private Pickable CheckPickable ( Vector2 offset, ref Pickable pickable ) {
		var collided = Physics2D.OverlapBoxAll( (Vector2)transform.position + offset, pickupSize, 0, pickableLayers );

		foreach( var col in collided ) {
			var p = col.GetComponent<Pickable>();

			if( p != null ) {
				if( pickable == null ) pickable = p;
				else if( p.priority > pickable.priority ) pickable = p;
			}
		}

		return pickable;
	}

	internal void InitHistoryForSpawn () {
		lastPosition = transform.position + Vector3.down * 0.3f;
		positionHistoryIndex = 0;
		for( int i = 0; i < positionHistory.Length; i++ ) {
			positionHistory[ i ] = lastPosition;
		}
	}

	public void DropHeldItem () {
		isHoldingItem = false;
		item.OnExitPickup();
		item.transform.parent = null;
		item.transform.position = transform.position;
		var knockback = fineFacing.normalized * 10;
		knockback = Quaternion.AngleAxis( Random.Range( -10, 10 ), Vector3.forward ) * knockback;
		item.Knockback( velocity + knockback );
		item = null;
	}

	private void TryUseHeldItem () {
		item.Use();
	}

	public void GrabPickable ( Pickable pickable ) {
		if( isHoldingItem ) {
			DropHeldItem();
		}

		isHoldingItem = true;
		item = pickable;
		pickable.OnEnterPickup( this );

		pickable.transform.parent = transform;
		UpdateHeldPosition();
	}

	public void GrabTree ( Tree pickable ) {
		PlaySound( pickupTree );
		isHoldingTree = true;
		pickable.OnEnterPickup( this );

		pickable.transform.parent = treeContainer;
		pickable.transform.localPosition = Vector3.zero;
		//UpdateHeldPosition();

		Game.instance.camera.target = transform;
	}

	public void PlaceTree () {
		PlaySound( putdownTree );
		isHoldingTree = false;
		tree.OnExitPickup();

		tree.transform.parent = null;

		tree.transform.position = WalkHistoryToDist( transform.position, treePlaceOffset, false );

		Game.instance.camera.target = tree.transform;
	}

	private void OnDrawGizmos () {
		if( !Application.isPlaying ) {
			Gizmos.color = new Color( 1, 0, 0, 0.3f );
			Gizmos.DrawWireCube( transform.position + (Vector3)pickupOffsetH, pickupSize );
			Gizmos.DrawWireCube( transform.position + (Vector3)pickupOffsetV, pickupSize );
			Gizmos.color = new Color( 0, 1, 0, 0.3f );
			Gizmos.DrawWireCube( transform.position, treePlaceOffset * 2 );

			Gizmos.DrawWireCube( transform.position + (Vector3)treeHoldPosition, Vector3.one * 0.1f );
			Gizmos.color = new Color( 0, 0, 1, 0.3f );
			Gizmos.DrawWireCube( transform.position + (Vector3)itemHoldPosition, Vector3.one * 0.1f );
		}
	}
}
