﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionType {
	None,
	Activate,
	Deactivate,
	ActivateNoReset,
	Spawn
}

public enum SpawnMode {
	None,
	Sequence,
	Random
}

public enum StartMode {
	None,
	Wait,
	Immediate
}

[Serializable]
public class RoundAction {
	public float delay;

	public ActionType type = ActionType.Activate;

	public EnemySpawner spawner;

	public SpawnMode spawnModeOverride;
	public EnemyAI[] spawnPrefabOverride;
	public float spawnDelayMinOverride;
	public float spawnDelayMaxOverride;
	public int spawnCountOverride;

	public StartMode startMode;

	[NonSerialized]
	public bool activated;

	public void Activate ( RoundSequencer seq ) {
		if( !activated ) {
			activated = true;

			spawner.Activate( this, seq );
		}
	}

	internal int CollectEnemyCount () {
		if( spawnCountOverride > 0 ) return spawnCountOverride;
		return spawner.spawnCount;
	}
}

public enum WinMode {
	AllEnemies,
	NumEnemies,
	Timer,
	Miniboss
}

public class RoundSequencer : MonoBehaviour {

	public WinMode winMode = WinMode.AllEnemies;

	public int killRequirement;

	public bool active;

	public List<RoundAction> actions;

	float roundTimer;

	[NonSerialized]
	public List<EnemyAI> enemies = new List<EnemyAI>();

	[NonSerialized]
	public int currentHighwater;
	[NonSerialized]
	public int currentKillTarget;
	[NonSerialized]
	public int currentKillCount;


	void Start () {
		currentHighwater = 0;
		currentKillTarget = 0;
		currentKillCount = 0;

		if( winMode == WinMode.AllEnemies ) {
			currentKillTarget = CollectEnemyCounts();
		} else if( winMode == WinMode.NumEnemies ) {
			currentKillTarget = killRequirement;
		}

		Game.instance.ui.UpdateRoundData( this );
	}

	void DetectWin () {
		if( winMode == WinMode.AllEnemies || winMode == WinMode.NumEnemies ) {
			if( currentKillCount >= currentKillTarget ) {
				Game.instance.EndRound();
			}
		}
	}

	private int CollectEnemyCounts () {
		var count = 0;

		foreach( var act in actions ) {
			count += act.CollectEnemyCount();
		}

		return count;
	}

	public void TrackEnemy ( EnemyAI ai ) {
		enemies.Add( ai );
		ai.onDestroy += OnEnemyDestroy;

		currentHighwater += 1;
	}

	void OnEnemyDestroy ( PhysicsBase ai ) {
		ai.onDestroy -= OnEnemyDestroy;
		enemies.Remove( (EnemyAI)ai );
		currentKillCount += 1;
	}
	
	void Update () {
		if( active ) {
			roundTimer += Time.deltaTime;

			for( int i = 0; i < actions.Count; i++ ) {
				var act = actions[ i ];
				if( act.activated ) continue;

				if( act.delay < roundTimer ) {
					act.Activate( this );
				}
			}

			DetectWin();

			Game.instance.ui.UpdateRoundData( this );
		}
	}

	internal void Cleanup () {

	}
}
