﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class SimpleSorter : MonoBehaviour {

	SortingGroup group;
	// Use this for initialization
	void Start () {
		group = GetComponentInChildren<SortingGroup>();	
	}
	
	// Update is called once per frame
	void Update () {
		group.sortingOrder = Mathf.RoundToInt( -transform.position.y * 100 );
	}
}
