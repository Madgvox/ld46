﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenWipe : MonoBehaviour {

	public RawImage image;

	public float flowSpeed = 2;

	RectTransform t;

	float targetWidth;

	float wVel;

	// Use this for initialization
	void Start () {
		t = (RectTransform)transform;
	}

	public void WipeStart () {
		targetWidth = 2000;
		Debug.Log( "WIPE START" );
	}

	public void WipeEnd () {
		targetWidth = 0;
		Debug.Log( "WIPE END" );
	}

	// Update is called once per frame
	void Update () {
		var rect = image.uvRect;
		rect.y += flowSpeed * Time.deltaTime;
		image.uvRect = rect;

		var size = t.sizeDelta;

		size.x = Mathf.SmoothDamp( size.x, targetWidth, ref wVel, 0.2f );
		if( size.x > 15f ) {
			image.enabled = true;
		} else {
			image.enabled = false;
		}
		t.sizeDelta = size;
	}
}
