﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSwap : MonoBehaviour {

	public Sprite[] sprites;

	SpriteRenderer sprite;

	public float swapTime = 0.033f;

	int index;
	float timer;
	// Use this for initialization
	void Start () {
		sprite = GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if( timer > swapTime ) {
			timer -= swapTime;

			index += 1;

			index = index % sprites.Length;

			sprite.sprite = sprites[ index ];
		}
	}
}
