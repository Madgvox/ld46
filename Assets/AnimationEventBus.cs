﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventBus : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SendEvent ( string eventName ) {
		GetComponentInParent<AnimationEventReciever>().RecieveEvent( eventName );
	}
}
