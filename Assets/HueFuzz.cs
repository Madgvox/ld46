﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HueFuzz : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var spr = GetComponent<SpriteRenderer>();
		var mat = new MaterialPropertyBlock();
		spr.GetPropertyBlock( mat );
		mat.SetColor( "_Color", new Color( Random.Range( 0.9f, 1f ), Random.Range( 0.9f, 1f ), Random.Range( 0.9f, 1f ), 1 ) );
		spr.SetPropertyBlock( mat );
	}
}
